package tw.teddysoft.ddd.model;

import tw.teddysoft.ddd.core.ValueObject;

import java.util.UUID;

public class WorkflowId extends ValueObject {

    private final String id;

    public WorkflowId(String id){
        super();
        this.id = id;
    }

    public WorkflowId(UUID id){
        super();
        this.id = id.toString();
    }

    public String value() {
        return id;
    }

    public static WorkflowId valueOf(String id){
        return new WorkflowId(id);
    }

    public static WorkflowId create(){
        return new WorkflowId(UUID.randomUUID().toString());
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof WorkflowId))
            return false;

        WorkflowId target = (WorkflowId) o;
        return target.id.equals(id) ;
    }

    @Override public int hashCode() {
        int result = 17;
        result = 31 * result + id.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return id;
    }
}
