package tw.teddysoft.ddd.model;

import tw.teddysoft.ddd.core.DomainEvent;

import java.time.Instant;
import java.util.UUID;

public class BoardEvents extends DomainEvent {

    public BoardEvents(Instant occurredOn) {
        super(occurredOn);
    }

    public BoardEvents(UUID id, Instant occurredOn) {
        super(id, occurredOn);
    }

    ///////////////////////////////////////////////////////////////
    public static class BoardCreated extends BoardEvents {

        private final String teamId;
        private final BoardId boardId;
        private final String boardName;

        public BoardCreated(
                String teamId,
                BoardId boardId,
                String boardName,
                UUID id,
                Instant occurredOn) {
            super(id, occurredOn);
            this.teamId = teamId;
            this.boardId = boardId;
            this.boardName = boardName;
        }

        public String getTeamId() {
            return teamId;
        }

        public BoardId getBoardId() {
            return boardId;
        }

        public String getBoardName() {
            return boardName;
        }
    }

    ///////////////////////////////////////////////////////////////
    public static class BoardMemberAdded extends BoardEvents {

        private final String userId;
        private final BoardId boardId;
        private final BoardRole boardRole;

        public BoardMemberAdded(
                String userId,
                BoardId boardId,
                BoardRole boardRole,
                UUID id,
                 Instant occurredOn) {
            super(id, occurredOn);
            this.userId = userId;
            this.boardId = boardId;
            this.boardRole = boardRole;
        }

        public String getUserId() {
            return userId;
        }

        public BoardId getBoardId() {
            return boardId;
        }

        public BoardRole getBoardRole() {
            return boardRole;
        }
    }

    ///////////////////////////////////////////////////////////////
    public static class BoardMemberRemoved extends BoardEvents {

        private final BoardId boardId;
        private final String userId;

        public BoardMemberRemoved(
                String userId,
                BoardId boardId,
                UUID id,
                Instant occurredOn) {
            super(id, occurredOn);
            this.userId = userId;
            this.boardId = boardId;
        }

        public BoardId getBoardId() {
            return boardId;
        }

        public String getUserId() {
            return userId;
        }
    }

    ///////////////////////////////////////////////////////////////
    public static class BoardRenamed extends BoardEvents {

        private final String teamId;
        private final BoardId boardId;
        private final String boardName;

        public BoardRenamed(
                String teamId,
                BoardId boardId,
                String boardName,
                UUID id,
                Instant occurredOn){
            super(id, occurredOn);
            this.teamId = teamId;
            this.boardId = boardId;
            this.boardName = boardName;
        }

        public String getTeamId() {
            return teamId;
        }

        public BoardId getBoardId() {
            return boardId;
        }

        public String getBoardName() {
            return boardName;
        }
    }

    ///////////////////////////////////////////////////////////////
    public static class WorkflowCommitted extends BoardEvents {

        private final BoardId boardId;
        private final WorkflowId workflowId;

        public WorkflowCommitted(
                BoardId boardId,
                WorkflowId workflowId,
                UUID id,
                Instant occurredOn) {
            super(id, occurredOn);
            this.boardId = boardId;
            this.workflowId = workflowId;
        }

        public BoardId getBoardId() {
            return boardId;
        }

        public WorkflowId getWorkflowId() {
            return workflowId;
        }
    }

    ///////////////////////////////////////////////////////////////
    public static class WorkflowUncommitted extends BoardEvents {

        private final BoardId boardId;
        private final WorkflowId workflowId;

        public WorkflowUncommitted(
                BoardId boardId,
                WorkflowId workflowId,
                UUID id,
                Instant occurredOn) {
            super(id, occurredOn);
            this.boardId = boardId;
            this.workflowId = workflowId;
        }

        public BoardId getBoardId() {
            return boardId;
        }

        public WorkflowId getWorkflowId() {
            return workflowId;
        }
    }
    ///////////////////////////////////////////////////////////////
    public static class WorkflowMoved extends BoardEvents {
        private final BoardId boardId;
        private final WorkflowId workflowId;
        private final String userId;
        private final int order;

        public WorkflowMoved(
                BoardId boardId,
                WorkflowId workflowId,
                String userId,
                int order,
                UUID id,
                Instant occurredOn) {
            super(id, occurredOn);
            this.boardId = boardId;
            this.workflowId = workflowId;
            this.userId = userId;
            this.order = order;
        }

        public BoardId getBoardId() {
            return boardId;
        }

        public WorkflowId getWorkflowId() {
            return workflowId;
        }

        public String getUserId() {
            return userId;
        }

        public int getOrder() {
            return order;
        }
    }

    ///////////////////////////////////////////////////////////////
    public static class BoardDeleted extends BoardEvents {

        private final String teamId;
        private final BoardId boardId;
        private final String userId;

        public BoardDeleted(
                String teamId,
                BoardId boardId,
                String userId,
                UUID id,
                Instant occurredOn) {
            super(id, occurredOn);
            this.teamId = teamId;
            this.boardId = boardId;
            this.userId = userId;
        }

        public String getTeamId() {
            return teamId;
        }

        public BoardId getBoardId() {
            return boardId;
        }

        public String getUserId() {
            return userId;
        }
    }

    ///////////////////////////////////////////////////////////////

}
