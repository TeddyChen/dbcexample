package tw.teddysoft.ddd.model;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class BoardDomainEventTest {
    private String userId = UUID.randomUUID().toString();

    private Board createBoard(){
        return new Board("teamId", BoardId.create(), "boardName");
    }

    private Board createBoardAndClearDomainEvent(){
        Board board = createBoard();
        board.clearDomainEvents();
        return board;
    }

    @Test
    public void create_a_board_publishes_a_board_created_domain_event() {

        Board board = createBoard();

        assertEquals(1, board.getDomainEvents().size());
        BoardEvents.BoardCreated domainEvent = (BoardEvents.BoardCreated)board.getDomainEvents().get(0);
        assertEquals(board.getTeamId(), domainEvent.getTeamId());
        assertEquals(board.getBoardId(), domainEvent.getBoardId());
        assertEquals(board.getName(), domainEvent.getBoardName());
    }

    @Test
    public void commit_a_workflow_publishes_a_workflow_committed_domain_event() {
        Board board = createBoardAndClearDomainEvent();
        WorkflowId workflowId = WorkflowId.create();

        board.commitWorkflow(workflowId);

        assertEquals(1, board.getDomainEvents().size());
        BoardEvents.WorkflowCommitted workflowCommitted = (BoardEvents.WorkflowCommitted) board.getDomainEvents().get(0);
        assertEquals(board.getBoardId(), workflowCommitted.getBoardId());
        assertEquals(workflowId, workflowCommitted.getWorkflowId());
    }

    @Test
    public void uncommit_a_workflow_publishes_a_workflow_uncommitted_domain_event() {
        Board board = createBoardAndClearDomainEvent();
        WorkflowId workflowId = WorkflowId.create();
        board.commitWorkflow(workflowId);
        board.clearDomainEvents();

        board.uncommitWorkflow(workflowId);

        assertEquals(1, board.getDomainEvents().size());

        BoardEvents.WorkflowUncommitted workflowUncommitted = (BoardEvents.WorkflowUncommitted) board.getDomainEvents().get(0);
        assertEquals(board.getBoardId(), workflowUncommitted.getBoardId());
        assertEquals(workflowId, workflowUncommitted.getWorkflowId());
    }

    @Test
    public void add_a_board_member_publishes_a_board_member_added_domain_event() {
        Board board = createBoardAndClearDomainEvent();

        board.becomeBoardMember(BoardRole.Member, userId);

        assertEquals(1, board.getDomainEvents().size());
        BoardEvents.BoardMemberAdded boardMemberAdded = (BoardEvents.BoardMemberAdded) board.getDomainEvents().get(0);
        assertEquals(board.getBoardId(), boardMemberAdded.getBoardId());
        assertEquals(board.getBoardMembers().get(0).getUserId(), boardMemberAdded.getUserId());
        assertEquals(board.getBoardMembers().get(0).getBoardRole(), boardMemberAdded.getBoardRole());
    }

    @Test
    public void remove_a_board_member_publishes_a_board_member_removed_domain_event() {
        Board board = createBoardAndClearDomainEvent();
        board.becomeBoardMember(BoardRole.Member, userId);
        board.clearDomainEvents();

        board.removeBoardMember(userId);

        assertEquals(1, board.getDomainEvents().size());
        BoardEvents.BoardMemberRemoved boardMemberRemoved = (BoardEvents.BoardMemberRemoved) board.getDomainEvents().get(0);
        assertEquals(board.getBoardId(), boardMemberRemoved.getBoardId());
        assertEquals(userId, boardMemberRemoved.getUserId());
    }


    @Test
    public void rename_a_board_publishes_a_board_renamed_domain_event() {
        Board board = createBoardAndClearDomainEvent();

        board.rename("newBoardName");

        assertEquals(1, board.getDomainEvents().size());
        BoardEvents.BoardRenamed boardRenamed = (BoardEvents.BoardRenamed) board.getDomainEvents().get(0);
        assertEquals(board.getBoardId(), boardRenamed.getBoardId());
        assertEquals(board.getName(), boardRenamed.getBoardName());
    }

}
